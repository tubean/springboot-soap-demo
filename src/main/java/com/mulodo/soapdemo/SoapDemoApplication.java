package com.mulodo.soapdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({ "com.mulodo.soapdemo.*" })
public class SoapDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SoapDemoApplication.class, args);
    }
}
